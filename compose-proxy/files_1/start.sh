#!/bin/sh

if [ $# -lt 1 ] ; then
	echo "Usage: ./start.sh <PORT>"
	exit 1
fi

TEST_PORT=$1

#docker run --rm -d hello-world
docker image  build -t  nginx-file-server .

docker run --rm -it -p $TEST_PORT:80 nginx-file-server
